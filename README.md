# README #

### IMPORTANT: ###
This is PETSc\_OGS, a fork of PETSc version 3.12.2. We claim no responsibility for the
work here. 

The purpose of this repo is to provide a version of PETSc compatible with PFLOTRAN\_OGS,
while incorporating key fixes that were introduced later than version 3.12.2.

If you want to use this to build PFLOTRAN\_OGS, see:

 * https://docs.opengosim.com/installing/ubuntu_install/

If you were looking for the original PETSc for any other purpose, see 
their repo:

 * https://gitlab.com/petsc/petsc

The original readme can be found in the file README_OLD.md.

