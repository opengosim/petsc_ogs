# README #

### IMPORTANT: ###
This is PETSc\_OGS, a fork of PETSc version 3.12.2. We claim no responsibility for the
work here. 

The purpose of this repo is to provide a version of PETSc compatible with PFLOTRAN\_OGS,
while incorporating key fixes that were introduced later than version 3.12.2.

See below for original readme, see README.md for the readme for this fork.

### What is this repository for? ###

Host the PETSc numerical library package. http://www.mcs.anl.gov/petsc

### How do I get set up? ###

* Download http://www.mcs.anl.gov/petsc/download/index.html
* Install http://www.mcs.anl.gov/petsc/documentation/installation.html

### Contribution guidelines ###

* See the file CONTRIBUTING
* https://gitlab.com/petsc/petsc/wikis/Home

### Who do I talk to? ###

* petsc-maint@mcs.anl.gov
* http://www.mcs.anl.gov/petsc/miscellaneous/mailing-lists.html
